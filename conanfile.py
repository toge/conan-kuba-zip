from conans import ConanFile, CMake, tools
import shutil

class KubaZipConan(ConanFile):
    name            = "kuba-zip"
    license         = "Unlicense"
    author          = "toge.mail@gmail.com"
    homepage        = "https://github.com/kuba--/zip"
    url             = "https://bitbucket.org/toge/conan-kuba-zip/"
    description     = "A portable, simple zip library written in C "
    topics          = ("zip", "compression", "miniz", "portable")
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = {"shared": False}
    generators      = "cmake"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        shutil.move("zip-{}".format(self.version), "zip")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_DISABLE_TESTING"] = True
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.configure(source_folder="zip")
        cmake.build()

    def package(self):
        self.copy("*.h",     dst="include", src="zip/src")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["zip"]

